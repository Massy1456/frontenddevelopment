const ul = document.createElement('ul');
const input = document.createElement('input');
const button = document.createElement('button');
button.textContent = 'Add Item';
const main = document.querySelector('.main');
main.append(input);
main.append(button);
main.append(ul);

const myStuff = ["Banana", "Apple", "Orange"];

myStuff.forEach((ele) => {
    createItem(ele);
})

button.addEventListener('click', (e) => {
    createItem(input.value);
    input.value = "";
})

function createItem(val){
    const ele = document.createElement('li');
    ele.textContent = val;
    const span = document.createElement('span');
    span.textContent = " X ";
    span.style.color = "red";
    span.addEventListener('click', (e)=> {
        ele.remove();
    })
    span.addEventListener('mouseover', (e)=> {
        span.style.cursor = 'grab';
    })

    span.addEventListener('mouseout', (e)=> {
        span.style.cursor = 'pointer';
    })

    ele.append(span);
    ul.append(ele);
}
